// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vant from 'vant'
import 'vant/lib/vant-css/index.css'
import axios from './request'
import { formatEmptyTime } from './filter'

// alert(window.navigator.cookieEnabled)

Vue.use(Vant)

Vue.config.productionTip = false
Vue.prototype.axios = axios

// 全局通用filter
Vue.filter('formatEmptyTime', formatEmptyTime)

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// })
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
