import axios from 'axios'
import Vue from 'vue'
const vm = new Vue()

axios.defaults.withCredentials = true
axios.interceptors.request.use(function (config) {
  return config
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.response.use(response => {
  // if (response.data.errno === undefined) return response
  const { result, value, message } = response.data
  const status = response.status

  // else if (result === '403') {
  //   vm.$toast('你还未登录')
  //   location.href = '/#login'
  //   location.reload()
  // }

  if (status === 200) {
    if (result === 'FAIL') {
      vm.$toast(message || '操作失败')
      return Promise.reject(message || '操作失败')
    } else {
      return value
    }
  } else {
    vm.$toast('与服务器连接失败')
    return Promise.reject(new Error('与服务器连接失败'))
  }
}, error => {
  if (error && error.response) {
    switch (error.response.status) {
      case 403:
        error.message = '没有访问权限'
        break
      case 500:
        error.message = '服务器发生错误了'
        break
      case 502:
        error.message = '连接服务器超时'
        break
      default:
    }
    vm.$toast(error.message)
  }
  return Promise.reject(error)
})

export default axios
