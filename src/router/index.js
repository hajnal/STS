import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/pages/welcome'
import Order from '@/pages/order'
import Home from '@/pages/home'
import Login from '@/pages/login'
import Register from '@/pages/register'
import Me from '@/pages/me'
import About from '@/pages/about'
import Feedback from '@/pages/feedback'
import Profile from '@/pages/profile'
import Account from '@/pages/account'
import Forget from '@/pages/forget'
import PublishComment from '@/pages/comment'
import PublishDeal from '@/parent/publishDeal'
import OrderDetail from '@/pages/OrderDetail'
import ParOrder from '@/parent/order'
import Check from '@/pages/check' // 审核页面
import Admin from '@/admin/admin'
import AdminCheck from '@/admin/check'
import AdminSuggest from '@/admin/suggest'
import Course from '@/pages/course' // 课程资料
import CourseDetail from '@/pages/courseDetail'

// 家长
import ParentProfile from '@/parent/profile'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Root',
      component: Welcome
    },
    {
      path: '/welcome',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/order',
      name: 'Order',
      component: Order
    },
    {
      path: '/parOrder',
      name: 'ParOrder',
      component: ParOrder
    },
    {
      path: '/me',
      name: 'Me',
      component: Me
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/feedback',
      name: 'Feedback',
      component: Feedback
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/forget',
      name: 'Forget',
      component: Forget
    },
    {
      path: '/comment',
      name: 'Comment',
      component: PublishComment
    },
    {
      path: '/check',
      name: 'Check',
      component: Check
    },
    {
      path: '/parent/profile',
      name: 'ParentProfile',
      component: ParentProfile
    },
    {
      path: '/publishDeal',
      name: 'PublishDeal',
      component: PublishDeal
    },
    {
      path: '/orderDetail',
      name: 'OrderDetail',
      component: OrderDetail
    },
    {
      path: '/admin',
      name: ' Admin',
      component: Admin
    },
    {
      path: '/admin/check',
      name: ' AdminCheck',
      component: AdminCheck
    },
    {
      path: '/admin/suggest',
      name: ' AdminSuggest',
      component: AdminSuggest
    },
    {
      path: '/course',
      name: 'Course',
      component: Course
    },
    {
      path: '/courseDetail',
      name: 'CourseDetail',
      component: CourseDetail
    }
  ]
})
