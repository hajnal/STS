const TIME = {
  1: '周一上午',
  2: '周一下午',
  3: '周二上午',
  4: '周二下午',
  5: '周三上午',
  6: '周三下午',
  7: '周四上午',
  8: '周四下午',
  9: '周五上午',
  10: '周五下午',
  11: '周六上午',
  12: '周六下午',
  13: '周日上午',
  14: '周日下午'
}
// 数字 -> 字符串
export function formatEmptyTime (time) {
  if (Number.isInteger(time)) {
    return TIME[time]
  }
  let timeArr = []
  let arr = Array.isArray(time) ? time : time.split(',')
  arr.forEach(key => {
    timeArr.push(TIME[key])
  })
  return timeArr.toString()
}
