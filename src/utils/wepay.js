import MD5 from 'md5'
const RandomString = require('randomstring')

const appId = ''
// key为商户平台设置的密钥key
const key = ''

function genSign (appId, nonceStr, prepayId, signType, timeStamp) {
  const stringA = `appId=${appId}&nonceStr=${nonceStr}&prepayId=${prepayId}&signType=${signType}&timeStamp=${timeStamp}`
  const stringSignTemp = `${stringA}&key=${key}`
  return MD5(stringSignTemp).toUpperCase()
}

export default function onBridgeReady (prepayId) {
  const nonceStr = RandomString.generate(32)
  const timeStamp = Date.now() / 1000
  WeixinJSBridge.invoke(
    'getBrandWCPayRequest', {
      'appId': appId, // 公众号名称，由商户传入
      'timeStamp': timeStamp, // 时间戳，自1970年以来的秒数
      'nonceStr': nonceStr, // 随机串
      'package': `prepay_id=${prepayId}`,
      'signType': 'MD5', // 微信签名方式
      'paySign': genSign(appId, nonceStr, prepayId, 'MD5', timeStamp) // 微信签名
    },
    function (res) {
      if (res.err_msg === 'get_brand_wcpay_request:ok') {} // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回,ok，但并不保证它绝对可靠
    }
  )
}
if (typeof WeixinJSBridge === 'undefined') {
  if (document.addEventListener) {
    document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
  } else if (document.attachEvent) {
    document.attachEvent('WeixinJSBridgeReady', onBridgeReady)
    document.attachEvent('onWeixinJSBridgeReady', onBridgeReady)
  }
} else {
  onBridgeReady()
}
