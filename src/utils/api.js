import { HOST } from './config'

const common = {
  getCode: `${HOST}/registerSMS`, // 获取二维码
  suggest: `${HOST}/suggest`,
  logout: `${HOST}/logout`,
  getUsers: `${HOST}/getAllStu`,
  getSuggests: `${HOST}/getSuggest`,
  openMatch: `${HOST}/openMatch`,
  closeMatch: `${HOST}/closeMatch`
}

const API = {
  // 学生
  1: {
    login: `${HOST}/stuLogin`,
    register: `${HOST}/stuRegister`,
    getUser: `${HOST}/getStuUser`,
    getUserById: `${HOST}/getStuUserByID`,
    updateUser: `${HOST}/updateStuUser`,
    freezeUser: `${HOST}/freezeStuUser`,
    activeUser: `${HOST}/activeStuUser`,
    uploadAvatar: `${HOST}/uploadHeadPic`,
    uploadPic: `${HOST}/uploadProvePic`,
    updateTime: `${HOST}/updateStuTime`,
    updateGrade: `${HOST}/updateStuGrade`,
    updateLesson: `${HOST}/updateStuLesson`,
    requestCheck: `${HOST}/waitPassStuUser`,
    getMatchOrders: `${HOST}/match`,
    getMyOrders: `${HOST}/stuGetDeal`,
    rushDeal: `${HOST}/rushDeal`,
    checkDeal: `${HOST}/checkdeal`,
    comment: `${HOST}/stuDealWord`,
    updatePassword: `${HOST}/stuForgetPassword`
  },
  // 家长
  2: {
    login: `${HOST}/parLogin`,
    register: `${HOST}/parRegister`,
    getUser: `${HOST}/getParUser`,
    getUserById: `${HOST}/getParUserByID`,
    updateUser: `${HOST}/updateParUser`,
    uploadAvatar: `${HOST}/uploadParHeadPic`,
    openMatch: `${HOST}/waitPassStuUser`,
    addDeal: `${HOST}/addDeal`,
    comment: `${HOST}/parDealWord`,
    updatePassword: `${HOST}/parForgetPassword`,
    getMyOrders: `${HOST}/parGetDeal`,
    delDeal: `${HOST}/parDelDeal`,
    endDeal: `${HOST}/endDeal`
  },
  // 管理员
  3: {
    freezeStu: `${HOST}/freezeStuUser`,
    activeStu: `${HOST}/activeStuUser`,
    freezePar: `${HOST}/freezeParUser`,
    activePar: `${HOST}/activeParUser`
  },
  ...common
}

export default API
